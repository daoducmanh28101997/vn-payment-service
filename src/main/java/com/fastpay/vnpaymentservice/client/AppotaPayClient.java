package com.fastpay.vnpaymentservice.client;

import com.fastpay.vnpaymentservice.dto.AppotPay.AppotaPayMakeTransferDto;
import com.fastpay.vnpaymentservice.dto.AppotPay.AppotaPayResponseDto;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;

@HttpExchange(accept = "application/json", contentType = "application/json")
public interface AppotaPayClient {

    @PostExchange("/api/v1/service/transfer/make")
    AppotaPayResponseDto callMakeTransfer(@RequestBody AppotaPayMakeTransferDto appotaPayMakeTransferDto, @RequestHeader("X-APPOTAPAY-AUTH") String header);

}
