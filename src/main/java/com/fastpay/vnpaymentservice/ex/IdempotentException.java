package com.fastpay.vnpaymentservice.ex;

import com.fastpay.vnpaymentservice.common.EnumErrorCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class IdempotentException extends Exception{
    EnumErrorCode enumErrorCode;

    public IdempotentException(EnumErrorCode errorCode) {
        this.enumErrorCode = errorCode;
    }

    public static IdempotentException conflict() {
        return new IdempotentException(EnumErrorCode.DUPLICATE_TRANSACTION);
    }
}
