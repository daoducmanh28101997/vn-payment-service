package com.fastpay.vnpaymentservice.ex;

import com.fastpay.vnpaymentservice.common.EnumErrorCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class AppotaPayException extends Exception {
    EnumErrorCode enumErrorCode;

    public AppotaPayException(EnumErrorCode errorCode) {
        this.enumErrorCode = errorCode;
    }

    public static AppotaPayException duplicateTransaction() {
        return new AppotaPayException(EnumErrorCode.DUPLICATE_TRANSACTION);
    }
}
