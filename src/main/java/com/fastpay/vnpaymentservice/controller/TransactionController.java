package com.fastpay.vnpaymentservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fastpay.vnpaymentservice.common.FastPayResponseEntity;
import com.fastpay.vnpaymentservice.dto.MakeTransferRequest;
import com.fastpay.vnpaymentservice.entity.TransactionEntity;
import com.fastpay.vnpaymentservice.service.TransferService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transaction")
@AllArgsConstructor
public class TransactionController {

    private TransferService transferService;

    @PostMapping("/make-transfer")
    public FastPayResponseEntity<TransactionEntity> makeTransfer(@RequestBody @Validated MakeTransferRequest makeTransferRequest, @RequestHeader(value = "idempotent-key") String idempotentKey) throws JsonProcessingException {
        return FastPayResponseEntity.ok(transferService.make(makeTransferRequest, idempotentKey));
    }

}
