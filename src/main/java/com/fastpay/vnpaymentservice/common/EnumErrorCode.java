package com.fastpay.vnpaymentservice.common;

import com.fastpay.vnpaymentservice.enums.FeeType;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Objects;

@Getter
public enum EnumErrorCode {
    BAD_REQUEST("PAYMENT-400","bad request"),
    INVALID_PARAM("PAYMENT-1001","Missing or Invalid Params"),
    DUPLICATE_TRANSACTION("PAYMENT-1002","Duplicate transaction"),
    CONFLICT_IDEMPOTENT_KEY("PAYMENT-1003","conflict idempotent!!!"),
    ERROR_CONNECT("PAYMENT-500","Error connect to payment gateway"),
    ;

    private final String errorCode;
    private final String message;

    EnumErrorCode(String errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public static EnumErrorCode from(String errorCode) {
        for (var e : EnumErrorCode.values()) {
            if (Objects.equals(e.getErrorCode(), errorCode)) {
                return e;
            }
        }
        throw new RuntimeException("Invalid enum value: " + errorCode);
    }

}
