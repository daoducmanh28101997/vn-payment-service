package com.fastpay.vnpaymentservice.common;

import com.fastpay.vnpaymentservice.ex.AppotaPayException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Log4j2
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(AppotaPayException.class)
    @ResponseStatus(HttpStatus.BAD_GATEWAY)
    public ResponseEntity<FastPayResponseEntity<?>> handler(AppotaPayException appotaPayException){
        log.error(appotaPayException);
        return ResponseEntity.badRequest().body(FastPayResponseEntity.init(appotaPayException.getEnumErrorCode(),null));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handler(Exception ex){
        ex.printStackTrace();
        log.error(ex);
        return ResponseEntity.internalServerError().body(ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.error(ex);
        ex.getBindingResult().getFieldErrors().forEach(fieldError -> {
            log.error("Invalid {} value submitted for {}",
                    fieldError.getRejectedValue(), fieldError.getField());
        });
        ex.fillInStackTrace();
        return super.handleMethodArgumentNotValid(ex, headers, status, request);
    }
}
