package com.fastpay.vnpaymentservice.common;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.annotation.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

import java.time.Instant;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Data
public class FastPayResponseEntity<T> {
    String errorCode = "0";
    String message = "success";
    T data;
    final Instant time = Instant.now();


    private FastPayResponseEntity(T data) {
        this.data = data;
    }

    private FastPayResponseEntity(EnumErrorCode errorCode, T data) {
        this.data = data;
        this.errorCode = errorCode.getErrorCode();
        this.message = errorCode.getMessage();
    }

    public static <T> FastPayResponseEntity<T> ok(@Nullable T data) {
       return new FastPayResponseEntity<>(data);
    }

    public static <T> FastPayResponseEntity<T> badRequest(@Nullable T data) {
        return new FastPayResponseEntity<T>(EnumErrorCode.BAD_REQUEST,data);
    }

    public static <T> FastPayResponseEntity<T>  init (EnumErrorCode errorCode,@Nullable T data) {
        return new FastPayResponseEntity<T>(errorCode,data);
    }
}
