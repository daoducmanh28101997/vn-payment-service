package com.fastpay.vnpaymentservice.config;

import com.fastpay.vnpaymentservice.client.AppotaPayClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ClientHttpRequest;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.client.*;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;
import reactor.core.publisher.Mono;

@Configuration
public class WebClientConfig {

    @Value("${appotapay.transfer.url}")
    private String transferUrl;

    @Bean
    AppotaPayClient appotaPayClient() {
        WebClient webClient = WebClient.builder().baseUrl(transferUrl)
                .filter((request, next) -> {
                    System.out.println("request URL: "+request.url());
                    System.out.println("request Header: "+request.headers());
                    return next.exchange(request);
                })
                .build();
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builder(WebClientAdapter.forClient(webClient)).build();

        return factory.createClient(AppotaPayClient.class);
    }
}
