package com.fastpay.vnpaymentservice.enums;

import java.util.Objects;

public enum FeeType {
    PAYER("payer"),RECEVIED("receiver");

    FeeType(String value) {
        this.value = value;
    }
    public static FeeType from(String value) {
        for (var e : FeeType.values()) {
            if (Objects.equals(e.value, value)) {
                return e;
            }
        }
        throw new RuntimeException("Invalid enum value: " + value);
    }

    private final String value;

    public String getValue(){
        return this.value;
    }


}
