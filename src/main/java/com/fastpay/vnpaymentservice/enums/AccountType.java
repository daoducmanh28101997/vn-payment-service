package com.fastpay.vnpaymentservice.enums;

import lombok.Data;

import java.util.Objects;

public enum AccountType {
    ACCOUNT("account"),
    CARD("card");
    private final String value;

    AccountType(String value) {
        this.value = value;
    }
    public static AccountType from(String value) {
        for (var e : AccountType.values()) {
            if (Objects.equals(e.value, value)) {
                return e;
            }
        }
        throw new RuntimeException("Invalid enum value: " + value);
    }

    public String getValue(){
        return this.value;
    }

}
