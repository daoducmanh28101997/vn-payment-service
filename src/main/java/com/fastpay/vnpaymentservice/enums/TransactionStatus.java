package com.fastpay.vnpaymentservice.enums;

import lombok.Getter;

import java.util.Objects;

@Getter
public enum TransactionStatus {

    SUCCESS("success"),PENDING("pending"),ERROR("error"), PROCESSING("processing");
    private final String value;

    TransactionStatus(String value) {
        this.value = value;
    }
    public static TransactionStatus from(String value) {
        for (var e : TransactionStatus.values()) {
            if (Objects.equals(e.getValue(), value)) {
                return e;
            }
        }
        throw new RuntimeException("Invalid enum value: " + value);
    }

}
