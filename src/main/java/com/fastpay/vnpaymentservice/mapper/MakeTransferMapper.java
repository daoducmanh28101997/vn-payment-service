package com.fastpay.vnpaymentservice.mapper;

import com.fastpay.vnpaymentservice.dto.AppotPay.AppotaPayMakeTransferDto;
import com.fastpay.vnpaymentservice.dto.MakeTransferRequest;
import com.fastpay.vnpaymentservice.dto.MakeTransferResponse;
import com.fastpay.vnpaymentservice.entity.TransactionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MakeTransferMapper {

    @Mapping(source = "refId", target = "partnerRefId")
    @Mapping(source = "feeType.value", target = "feeType")
    @Mapping(source = "accountType.value", target = "accountType")
    AppotaPayMakeTransferDto toAppotaPayMakeTransferDto(MakeTransferRequest makeTransferRequest);

}
