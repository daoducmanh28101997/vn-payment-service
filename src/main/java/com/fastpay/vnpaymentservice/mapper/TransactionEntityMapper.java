package com.fastpay.vnpaymentservice.mapper;

import com.fastpay.vnpaymentservice.dto.MakeTransferRequest;
import com.fastpay.vnpaymentservice.entity.TransactionEntity;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface TransactionEntityMapper {
    TransactionEntity toEntity(MakeTransferRequest makeTransferRequest);

    MakeTransferRequest toDto(TransactionEntity transactionEntity);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    TransactionEntity partialUpdate(MakeTransferRequest makeTransferRequest, @MappingTarget TransactionEntity transactionEntity);
}