package com.fastpay.vnpaymentservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fastpay.vnpaymentservice.dto.MakeTransferRequest;
import com.fastpay.vnpaymentservice.entity.TransactionEntity;

public interface TransferService {
    TransactionEntity make(MakeTransferRequest makeTransferRequest, String idempotentKey) throws JsonProcessingException;
    void updateStatus();
    void getAccountInfo();
}
