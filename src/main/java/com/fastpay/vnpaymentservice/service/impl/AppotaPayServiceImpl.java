package com.fastpay.vnpaymentservice.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fastpay.vnpaymentservice.client.AppotaPayClient;
import com.fastpay.vnpaymentservice.dto.AppotPay.AppotaPayMakeTransferDto;
import com.fastpay.vnpaymentservice.dto.AppotPay.AppotaPayResponseDto;
import com.fastpay.vnpaymentservice.dto.MakeTransferRequest;
import com.fastpay.vnpaymentservice.dto.MakeTransferResponse;
import com.fastpay.vnpaymentservice.entity.IdempotentCachedEntity;
import com.fastpay.vnpaymentservice.entity.TransactionEntity;
import com.fastpay.vnpaymentservice.enums.TransactionStatus;
import com.fastpay.vnpaymentservice.ex.AppotaPayException;
import com.fastpay.vnpaymentservice.ex.IdempotentException;
import com.fastpay.vnpaymentservice.mapper.MakeTransferMapper;
import com.fastpay.vnpaymentservice.mapper.TransactionEntityMapper;
import com.fastpay.vnpaymentservice.repository.IdempotentCachedEntityRepository;
import com.fastpay.vnpaymentservice.repository.TransactionEntityRepository;
import com.fastpay.vnpaymentservice.service.TransferService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Optional;

import static java.nio.charset.StandardCharsets.UTF_8;

@Service
@Log4j2
public class AppotaPayServiceImpl implements TransferService {

    private final TransactionEntityRepository transactionEntityRepository;
    private final IdempotentCachedEntityRepository idempotentCachedEntityRepository;
    private final ObjectMapper objectMapper;
    private final MakeTransferMapper makeTransferMapper;
    private final AppotaPayClient appotaPayClient;
    private final TransactionEntityMapper transactionEntityMapper;

    private final String apiKey;
    private final String secretKey;

    public AppotaPayServiceImpl(TransactionEntityRepository transactionEntityRepository,
                                IdempotentCachedEntityRepository idempotentCachedEntityRepository,
                                ObjectMapper objectMapper,
                                MakeTransferMapper makeTransferMapper,
                                AppotaPayClient appotaPayClient,
                                TransactionEntityMapper transactionEntityMapper,
                                @Value("${appotapay.api-key}") String apiKey,
                                @Value("${appotapay.secret-key}") String secretKey) {
        this.transactionEntityRepository = transactionEntityRepository;
        this.idempotentCachedEntityRepository = idempotentCachedEntityRepository;
        this.objectMapper = objectMapper;
        this.makeTransferMapper = makeTransferMapper;
        this.appotaPayClient = appotaPayClient;
        this.transactionEntityMapper = transactionEntityMapper;
        this.apiKey = apiKey;
        this.secretKey = secretKey;
    }

    @Override
    @SneakyThrows
    public TransactionEntity make(MakeTransferRequest makeTransferRequest, String idempotentKey) {


//        if (!bankListEntityRepository.existsByBankCodeIgnoreCase(makeTransferRequest.bankCode())) {
//            throw new RuntimeException();
//        }

        String strBody = objectMapper.writeValueAsString(makeTransferRequest);
        String hashed = DigestUtils.md5DigestAsHex(strBody.getBytes(UTF_8));
        Optional<IdempotentCachedEntity> optionalIdempotentCached = idempotentCachedEntityRepository.findById(idempotentKey);
        if (optionalIdempotentCached.isPresent()) {
            IdempotentCachedEntity idempotentCachedEntity = optionalIdempotentCached.get();
            if (idempotentCachedEntity.getHash().equals(hashed)) {
                return null;
            } else {
                throw IdempotentException.conflict();
            }
        } else {
            IdempotentCachedEntity idempotentCachedEntity = IdempotentCachedEntity.builder().id(idempotentKey).hash(hashed).refId(makeTransferRequest.refId()).build();
            idempotentCachedEntityRepository.saveAndFlush(idempotentCachedEntity);
        }

        AppotaPayMakeTransferDto transferDto = makeTransferMapper.toAppotaPayMakeTransferDto(makeTransferRequest);
        TransactionEntity entity = transactionEntityMapper.toEntity(makeTransferRequest);
        entity.setIdempotentKey(idempotentKey);
        entity.setTransactionStatus(TransactionStatus.PROCESSING);
        TransactionEntity transactionEntity = transactionEntityRepository.saveAndFlush(entity);
        String dataPreSignForMakeTransfer = transferDto.getDataPreSignForMakeTransfer();
        log.info("dataPreSignForMakeTransfer: " + dataPreSignForMakeTransfer);

        String signature = sign(dataPreSignForMakeTransfer, secretKey);
        log.info("signature: " + signature);
        transferDto.setSignature(signature);
        log.info("transferDto: " + objectMapper.writeValueAsString(transferDto));
        try {
            String token = getToken(apiKey, secretKey);
            log.info("Token : " + token);
            AppotaPayResponseDto transactionRes = appotaPayClient.callMakeTransfer(transferDto, token);
            log.info("transactionRes: " + objectMapper.writeValueAsString(transactionRes));
            transactionEntity.setTransactionId(transactionRes.getTransaction().getAppotapayTransId());
            transactionEntity.setTransferAmount(transactionRes.getTransaction().getTransferAmount());
            transactionEntity.setTransactionErrorCode(transactionRes.getErrorCode().toString());
            transactionEntity.setTransactionStatus(TransactionStatus.SUCCESS);
            transactionEntityRepository.saveAndFlush(transactionEntity);
            log.info(transactionEntity);
            return transactionEntity;

        } catch (WebClientResponseException ex) {
            ex.printStackTrace();
            AppotaPayResponseDto appotaPayResponseDto = objectMapper.readValue(ex.getResponseBodyAsString(), AppotaPayResponseDto.class);
            log.info(appotaPayResponseDto);

            switch (appotaPayResponseDto.getErrorCode().intValue()) {
                case 31:
                    log.info("Trung ma giao dich");
                    throw AppotaPayException.duplicateTransaction();
                case 32:
                    log.info("Trung ma giao dich");
                    throw AppotaPayException.duplicateTransaction();
                default:
            }

            throw ex;
        }

    }

    @Override
    public void updateStatus() {

    }

    @Override
    public void getAccountInfo() {

    }

    public static String sign(String data, String secretKey) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeyException {
        Mac mac = Mac.getInstance("HmacSHA256");
        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(UTF_8), "HmacSHA256");
        mac.init(secretKeySpec);
        return Hex.encodeHexString(mac.doFinal(data.getBytes(UTF_8)));
    }

    public String getToken(String apiKey, String secretKey) {
        long exp = Instant.now().toEpochMilli() / 1000 + 10000;
        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setHeaderParam("alg", "HS256")
                .setHeaderParam("cty", "appotapay-api;v=1")
                .claim("iss", "FASTPAY")
                .claim("api_key", apiKey)
                .claim("jti", apiKey + exp)
                .claim("exp", exp)
                .signWith(SignatureAlgorithm.HS256, secretKey.getBytes())
                .compact();

    }

}
