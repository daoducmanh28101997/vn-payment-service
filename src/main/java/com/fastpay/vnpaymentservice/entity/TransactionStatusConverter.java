package com.fastpay.vnpaymentservice.entity;

import com.fastpay.vnpaymentservice.enums.AccountType;
import com.fastpay.vnpaymentservice.enums.TransactionStatus;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
@Converter(autoApply = true)

public class TransactionStatusConverter implements AttributeConverter<TransactionStatus, String>  {
    @Override
    public String convertToDatabaseColumn(TransactionStatus transactionStatus) {
        return transactionStatus.getValue();
    }

    @Override
    public TransactionStatus convertToEntityAttribute(String s) {
        return TransactionStatus.from(s);
    }
}
