package com.fastpay.vnpaymentservice.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "transaction_log")
public class TransactionLogEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Size(max = 200)
    @Column(name = "ref_id", length = 200)
    private String refId;

    @Size(max = 500)
    @Column(name = "request_header", length = 500)
    private String requestHeader;

    @Size(max = 500)
    @Column(name = "request_body", length = 500)
    private String requestBody;

    @Size(max = 20)
    @Column(name = "request_url", length = 20)
    private String requestUrl;

    @Size(max = 500)
    @Column(name = "response_body", length = 500)
    private String responseBody;

    @Size(max = 10)
    @Column(name = "response_http_status", length = 10)
    private String responseHttpStatus;

    @Column(name = "created_time")
    private Instant createdTime;

    @Column(name = "updated_time")
    private Instant updatedTime;

    @Size(max = 20)
    @Column(name = "created_by", length = 20)
    private String createdBy;

    @Size(max = 20)
    @Column(name = "updated_by", length = 20)
    private String updatedBy;

}