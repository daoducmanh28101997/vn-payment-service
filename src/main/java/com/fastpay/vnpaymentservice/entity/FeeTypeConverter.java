package com.fastpay.vnpaymentservice.entity;

import com.fastpay.vnpaymentservice.enums.AccountType;
import com.fastpay.vnpaymentservice.enums.FeeType;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class FeeTypeConverter implements AttributeConverter<FeeType, String> {
    @Override
    public String convertToDatabaseColumn(FeeType feeType) {
        return feeType.getValue();
    }

    @Override
    public FeeType convertToEntityAttribute(String s) {
        return FeeType.from(s);
    }
}
