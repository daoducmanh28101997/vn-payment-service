package com.fastpay.vnpaymentservice.entity;

import com.fastpay.vnpaymentservice.enums.AccountType;
import com.fastpay.vnpaymentservice.enums.FeeType;
import com.fastpay.vnpaymentservice.enums.TransactionStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;

@Getter
@Setter
@ToString
@Entity
@Table(name = "transaction", indexes = {
        @Index(name = "ref_id_idempotent_key_unique", columnList = "ref_id, idempotent_key", unique = true)
})
public class TransactionEntity {
    @Id
    @Size(max = 36)
    @Column(name = "id", nullable = false, length = 36)
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Size(max = 100)
    @NotNull
    @Column(name = "ref_id", nullable = false, length = 100)
    private String refId;

    @Size(max = 36)
    @NotNull
    @Column(name = "idempotent_key", nullable = false, length = 36)
    private String idempotentKey;

    @Size(max = 20)
    @NotNull
    @Column(name = "bank_code", nullable = false, length = 20)
    private String bankCode;

    @Size(max = 50)
    @NotNull
    @Column(name = "account_no", nullable = false, length = 50)
    private String accountNo;

    @NotNull
    @Column(name = "account_type", nullable = false)
    @Convert(converter = AccountTypeConverter.class)
    private AccountType accountType;

    @Size(max = 100)
    @NotNull
    @Column(name = "account_name", nullable = false, length = 100)
    private String accountName;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Integer amount;

    @NotNull
    @Column(name = "fee_type", nullable = false)
    @Convert(converter = FeeTypeConverter.class)
    private FeeType feeType;

    @Size(max = 200)
    @Column(name = "message", length = 200)
    private String message;

    @Size(max = 20)
    @Column(name = "customer_phone_number", length = 20)
    private String customerPhoneNumber;

    @Size(max = 100)
    @Column(name = "contract_number", length = 100)
    private String contractNumber;

    @Column(name = "transaction_status")
    @Convert(converter = TransactionStatusConverter.class)
    private TransactionStatus transactionStatus;

    @Column(name = "transfer_amount")
    private Integer transferAmount;

    @Size(max = 100)
    @Column(name = "transaction_id", length = 100)
    private String transactionId;

    @Size(max = 10)
    @Column(name = "transaction_error_code", length = 10)
    private String transactionErrorCode;

    @Column(name = "transaction_time")
    private Instant transactionTime;

    @Column(name = "created_time")
    private Instant createdTime;

    @Column(name = "updated_time")
    private Instant updatedTime;

    @Size(max = 20)
    @Column(name = "created_by", length = 20)
    private String createdBy;

    @Size(max = 20)
    @Column(name = "updated_by", length = 20)
    private String updatedBy;

}