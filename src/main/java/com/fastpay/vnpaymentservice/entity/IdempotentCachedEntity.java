package com.fastpay.vnpaymentservice.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

@Getter
@Setter
@Entity
@Table(name = "idempotent_cached")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IdempotentCachedEntity {
    @Id
    @Size(max = 36)
    @Column(name = "id", nullable = false, length = 36)
    private String id;

    @Size(max = 1000)
    @NotNull
    @Column(name = "hash", nullable = false, length = 1000)
    private String hash;

    @Size(max = 100)
    @NotNull
    @Column(name = "ref_id", nullable = false, length = 100)
    private String refId;

    @Size(max = 10)
    @Column(name = "code", length = 10)
    private String code;

    @Size(max = 255)
    @Column(name = "message")
    private String message;

    @Size(max = 100)
    @Column(name = "transaction_id", length = 100)
    private String transactionId;

}