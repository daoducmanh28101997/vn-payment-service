package com.fastpay.vnpaymentservice.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "bank_list", indexes = {
        @Index(name = "bank_code", columnList = "bank_code", unique = true),
        @Index(name = "bank_list_bank_name_index", columnList = "bank_name"),
        @Index(name = "bank_list_bank_tradding_name_index", columnList = "bank_tradding_name")
})
public class BankListEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Size(max = 20)
    @NotNull
    @Column(name = "bank_code", nullable = false, length = 20)
    private String bankCode;

    @Size(max = 255)
    @Column(name = "bank_name")
    private String bankName;

    @Size(max = 200)
    @NotNull
    @Column(name = "bank_tradding_name", nullable = false, length = 200)
    private String bankTraddingName;

}