package com.fastpay.vnpaymentservice.entity;

import com.fastpay.vnpaymentservice.enums.AccountType;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class AccountTypeConverter implements AttributeConverter<AccountType, String> {
    @Override
    public String convertToDatabaseColumn(AccountType accountType) {
        return accountType.getValue();
    }

    @Override
    public AccountType convertToEntityAttribute(String s) {
        return AccountType.from(s);
    }
}
