package com.fastpay.vnpaymentservice.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fastpay.vnpaymentservice.enums.AccountType;
import com.fastpay.vnpaymentservice.enums.FeeType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public record MakeTransferResponse(
        String refId,
        @Schema(description = "Bank Code", defaultValue = "APPOTA_TEST")
        String bankCode,
        @Schema(description = "Account number", defaultValue = "9704000000000018")
        String accountNo,
        @Schema(description = "Account type", enumAsRef = true, defaultValue = "CARD")

        AccountType accountType,
        @Schema(description = "amount transfer", defaultValue = "10000")
        Integer amount,
        @Schema(description = "fee type", defaultValue = "PAYER", enumAsRef = true)
        FeeType feeType,
        @Schema(description = "payment provider transaction id", example = "01HAKWYPBX8HVBWFQ72DK8VSXW")
        String transactionId,
        @Schema(description = "provider transfer amount", example = "10000")
        String transferAmount,
        @Schema(description = "provider time transfer", example = "18-09-2023 17:21:38")
        String transactionTime,
        String transactionErrorCode,

        String transactionStatus) {
}
