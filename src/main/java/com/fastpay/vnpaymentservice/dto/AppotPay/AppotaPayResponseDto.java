
package com.fastpay.vnpaymentservice.dto.AppotPay;

import lombok.Data;

@Data
public class AppotaPayResponseDto {

    
    private Account account;
    
    private Long errorCode;
    
    private String message;
    
    private String signature;
    
    private Transaction transaction;

    private AccountInfo accountInfo;

}
