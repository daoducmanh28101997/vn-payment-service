package com.fastpay.vnpaymentservice.dto.AppotPay;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fastpay.vnpaymentservice.enums.FeeType;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Optional;

@Data
public class AppotaPayMakeTransferDto {
    @NotNull @NotBlank @Size(max = 100) String partnerRefId;
    @NotNull @NotBlank @Size(max = 10) String bankCode;
    @NotNull @NotBlank @Size(max = 22) String accountNo;
    @NotNull @NotBlank @Size(max = 50) String accountType;
    @NotNull @NotBlank @Size(max = 100) String accountName;
    @NotNull Integer amount;
    @NotNull @Size(max = 50) String feeType;
    @Size(max = 200) String message;
    String signature;

    @JsonIgnore
    public String getDataPreSignForMakeTransfer() {
        return String.format("accountName=%s&accountNo=%s&accountType=%s&amount=%s&bankCode=%s&feeType=%s&message=%s&partnerRefId=%s",
                accountName, accountNo, accountType, amount, bankCode, feeType, Optional.ofNullable(message).orElse(""), partnerRefId);
    }

}
