
package com.fastpay.vnpaymentservice.dto.AppotPay;

import lombok.Data;

@Data
public class Transaction {

    private Long amount;
    private String appotapayTransId;
    private String time;
    private Integer transferAmount;

}
