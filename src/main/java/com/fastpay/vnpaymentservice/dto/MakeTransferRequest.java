package com.fastpay.vnpaymentservice.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fastpay.vnpaymentservice.enums.AccountType;
import com.fastpay.vnpaymentservice.enums.FeeType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serializable;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public record MakeTransferRequest(
        @Schema( description = "Transction Reference ID", defaultValue = "5574a20f-ee70-49a8-a32a-5d0706ddca32")
        @NotNull @NotBlank @Size(max = 100) String refId,
        @Schema( description = "Bank Code", defaultValue = "APPOTA_TEST")
        @NotNull @NotBlank @Size(max = 20) String bankCode,
        @Schema( description = "Account number", defaultValue = "9704000000000018")
        @NotNull @NotBlank @Size(max = 50) String accountNo,
        @Schema( description = "Account type", enumAsRef = true, defaultValue = "CARD")

        @NotNull AccountType accountType,
        @Schema( description = "Account Name", defaultValue = "AP APPOTAPAY")
        @NotNull @NotBlank @Size(max = 100) String accountName,
        @Schema( description = "amount transfer", defaultValue = "10000")
        @NotNull @Min(value = 10000,message = "min is 10000") Integer amount,
        @Schema( description = "fee type", defaultValue = "PAYER", enumAsRef = true)
        @NotNull FeeType feeType,
        @Size(max = 200) String message) implements Serializable {
}