package com.fastpay.vnpaymentservice.repository;

import com.fastpay.vnpaymentservice.entity.TransactionLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionLogEntityRepository extends JpaRepository<TransactionLogEntity, Integer> {
}