package com.fastpay.vnpaymentservice.repository;

import com.fastpay.vnpaymentservice.entity.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntityRepository extends JpaRepository<TransactionEntity, String> {
}