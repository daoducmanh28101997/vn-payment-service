package com.fastpay.vnpaymentservice.repository;

import com.fastpay.vnpaymentservice.entity.BankListEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankListEntityRepository extends JpaRepository<BankListEntity, Integer> {
    boolean existsByBankCodeIgnoreCase(String bankCode);
}