package com.fastpay.vnpaymentservice.repository;

import com.fastpay.vnpaymentservice.entity.IdempotentCachedEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IdempotentCachedEntityRepository extends JpaRepository<IdempotentCachedEntity, String> {
}