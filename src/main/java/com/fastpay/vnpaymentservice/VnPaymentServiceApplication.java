package com.fastpay.vnpaymentservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.reactive.ReactiveUserDetailsServiceAutoConfiguration;

@SpringBootApplication
public class VnPaymentServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(VnPaymentServiceApplication.class, args);
    }

}
