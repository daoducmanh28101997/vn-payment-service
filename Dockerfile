# Stage 1: Build
FROM maven:3.8.4-openjdk-17-slim AS build
WORKDIR /app
COPY pom.xml .
RUN mvn dependency:go-offline
COPY src ./src
RUN mvn package

# Stage 2: Run
FROM openjdk:17-slim
WORKDIR /app
COPY --from=build /app/target/vn-payment-service-0.0.1-SNAPSHOT.jar ./my-app.ja
EXPOSE 8080
CMD ["java", "-jar", "my-app.jar"]
